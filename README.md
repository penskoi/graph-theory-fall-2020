# Graph theory / Теория графов

Main repository / Основной репозиторий с материалами курса

# Лекции / практики

## Занятие 1
- Видео лекции: https://www.youtube.com/watch?v=myiSjTa1jkk
- Слайды: https://logic.pdmi.ras.ru/~dvk/ITMO/Lections/1_basic.pdf
- Практические задания / домашняя работа: https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser1.pdf
- Видео практики: https://www.youtube.com/watch?v=WSAMxCUZAGk

## Занятие 2
- Видео лекции: https://www.youtube.com/watch?v=pd3kLosZ6rI
- Слайды: 
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Lections/1_basic.pdf
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Lections/2_cycles.pdf
- Практические задания / домашняя работа: 
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser1.pdf
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser2.pdf
- Видео практики:
  - https://www.youtube.com/watch?v=CuYYXT1zLzI

## Занятие 3
- Видео лекции: https://www.youtube.com/watch?v=eZcPatjYA-g
- Слайды: https://logic.pdmi.ras.ru/~dvk/ITMO/Lections/3_matchings.pdf 
- Практические задания / домашняя работа:
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser2.pdf
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser3.pdf
- Видео практики:

## Занятие 4
- Видео лекции: 
- Слайды: https://logic.pdmi.ras.ru/~dvk/ITMO/Lections/3_matchings.pdf 
- Практические задания / домашняя работа:
  - https://logic.pdmi.ras.ru/~dvk/ITMO/Problems/ser4.pdf
- Видео практики:
